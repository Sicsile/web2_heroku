const status = document.querySelector('#status');
const latlong = document.querySelector('#latlong');
var ime = document.currentScript.getAttribute('data-name');
var email = document.currentScript.getAttribute('data-email');
var time = document.currentScript.getAttribute('data-time');

// Dohvat lokacije
if (!navigator.geolocation) {
    status.textContent = 'Geolocation is not supported by your browser';
} else {
    navigator.geolocation.getCurrentPosition(success, error);
}

async function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    ///////////////////////////////////////
    const data = {
        ime,
        email,
        time,
        latitude,
        longitude
    };
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('../../', options);
    const newData = await response.json();


    //////////////////////////////////////////////
    //Izrada mape, markera i tileova
    const mymap = L.map('map').setView([0, 0], 1);
    const marker = L.marker([0, 0]).addTo(mymap);
    const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    const attribution =
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
    const tiles = L.tileLayer(tileUrl, {
        attribution
    });
    tiles.addTo(mymap);

    //Dodavanje teksta lat i long

    const zoom = 13;
    latlong.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;
    mymap.setView([latitude, longitude], zoom);

    //Dodavanje markera
    marker.setLatLng([latitude, longitude]);

    ///////////////////////////////
    console.log(newData);
    console.log(newData.length);
    if ("greska" in newData) {
        console.log("refresh");
        msg = "<p>Refresh was done" + "</p>";
        document.querySelector('#status2').innerHTML += msg;
    } else{
        var len = newData.length;
        var i;
        msg = "<p>Found rows: " + len + "</p>";
        document.querySelector('#status2').innerHTML += msg;
        if(len > 5){
            len = 5;
        }
        for (i = 0; i < len; i++) {
            const newmarker = L.marker([0, 0]).addTo(mymap);

            msg = "<p><b>" +
                newData[i].username + " | " +
                newData[i].email + " | " +
                newData[i].logintime + " | " +
                newData[i].latitude + " | " +
                newData[i].longitude +
                "</b></p>";
            document.querySelector('#status2').innerHTML += msg;

            //Dodavanje markera
            newmarker.setLatLng([newData[i].latitude, newData[i].longitude]);
            newmarker.bindPopup(
                newData[i].username + " " +
                newData[i].email + " " +
                newData[i].logintime + " " +
                newData[i].latitude + " " +
                newData[i].longitude);
        }
    }
    // var len = newData.length;
    // var i;
    // msg = "<p>Found rows: " + len + "</p>";
    // document.querySelector('#status2').innerHTML += msg;

    // for (i = 0; i < len; i++) {
    //     const newmarker = L.marker([0, 0]).addTo(mymap);

    //     msg = "<p><b>" +
    //         newData[i].username + " | " +
    //         newData[i].email + " | " +
    //         newData[i].logintime + " | " +
    //         newData[i].latitude + " | " +
    //         newData[i].longitude +
    //         "</b></p>";
    //     document.querySelector('#status2').innerHTML += msg;

    //     //Dodavanje markera
    //     newmarker.setLatLng([newData[i].latitude, newData[i].longitude]);
    //     newmarker.bindPopup(
    //         newData[i].username + " " +
    //         newData[i].email + " " +
    //         newData[i].logintime + " " +
    //         newData[i].latitude + " " +
    //         newData[i].longitude);
    // }
    ///////////////////////////////
    //stvaranje/dodavanje u bazu podataka u pregledniku
    // var db = openDatabase('userdb', '1.0', 'Testing', 2 * 1024 * 1024);
    // var msg;


    // //insert            
    // db.transaction(function (tx) {
    //     tx.executeSql('create table if not exists logs (name, email, time unique, lat, long)');
    //     tx.executeSql('insert into logs (name, email, time, lat, long) values (?, ?, ?, ?, ?)', [
    //         ime,
    //         email, time, `${latitude}`, `${longitude}`
    //     ]);
    //     msg = '<p>Log message created and row inserted.</p>';
    //     document.querySelector('#status2').innerHTML = msg;

    // });

    // //read
    // db.transaction(function (tx) {
    //     tx.executeSql('select * from logs order by time desc limit 5', [], function (tx, results) {
    //         var len = results.rows.length;
    //         var i;
    //         msg = "<p>Found rows: " + len + "</p>";
    //         document.querySelector('#status2').innerHTML += msg;

    //         for (i = 0; i < len; i++) {
    //             const newmarker = L.marker([0, 0]).addTo(mymap);

    //             msg = "<p><b>" +
    //                 results.rows.item(i).name + " | " +
    //                 results.rows.item(i).email + " | " +
    //                 results.rows.item(i).time + " | " +
    //                 results.rows.item(i).lat + " | " +
    //                 results.rows.item(i).long +
    //                 "</b></p>";
    //             document.querySelector('#status2').innerHTML += msg;

    //             //Dodavanje markera
    //             newmarker.setLatLng([parseFloat(results.rows.item(i).lat), parseFloat(
    //                 results
    //                 .rows.item(i).long)]);
    //             newmarker.bindPopup(results.rows.item(i).name + " " +
    //                 results.rows.item(i).email + " " +
    //                 results.rows.item(i).time + " " +
    //                 results.rows.item(i).lat + " " +
    //                 results.rows.item(i).long);
    //         }
    //     });
    // }, null);
}

function error() {
    status.textContent = 'Unable to retrieve your location';
}