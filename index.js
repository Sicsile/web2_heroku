const express = require('express');
const app = express();
require('dotenv').config();
const {
    auth,
    requiresAuth
} = require('express-openid-connect');
const https = require('https');
const path = require('path');
const fs = require('fs');

const Client = require('pg').Client;

app.use(express.json());

var options = {
    key: fs.readFileSync(path.join(__dirname, 'cert', 'key.pem')),
    cert: fs.readFileSync(path.join(__dirname, 'cert', 'cert.pem'))
};

var array = [];


//middleware
app.use(
    auth({
        authRequired: false,
        auth0Logout: true,
        issuerBaseURL: process.env.ISSUER_BASE_URL,
        baseURL: process.env.BASE_URL,
        clientID: process.env.CLIENT_ID,
        secret: process.env.SECRET,

    })
);

app.use(express.static("public"));
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
    if (req.oidc.isAuthenticated()) {
        //////////////////////////////////
        // execute(req.oidc.user);
        //////////////////////////////////


        req.user = {
            isAuthenticated: req.oidc.isAuthenticated()
        };

        req.user.name = req.oidc.user.name;
        res.render('index', {
            user: req.user,
            name: req.oidc.user.nickname,
            email: req.oidc.user.email,
            time: req.oidc.user.updated_at,
            url: process.env.BASE_URL
        });
    } else {
        res.render('indexUnauthenticated', {
            url: process.env.BASE_URL
        });
    }


});

app.post('/', function (req, res) {
    console.log(req.body);
    const data = req.body;
    if ((array.length >= 1 && array[0].logintime !== data.time) || array.length == 0) {
        var found = false;
        var len = array.length;
        if(len > 5){
            len = 5;
        }
        for (var i = 0; i < len; i++) {
            if (array[i].logintime === data.time &&
                array[i].username === data.ime                ) {
                found = true;
                break;
            }
        }
        if(!found){
            array.unshift({
                "username": data.ime,
                "email": data.email,
                "logintime": data.time,
                "latitude": data.latitude,
                "longitude": data.longitude
            });
        }
        
    }


    res.json(array);
    // execute(data)
    //     .then((rows) => {
    //         if (rows != 0) {
    //             console.log(rows);
    //             console.log(rows.length);
    //             // res.json({
    //             //     status: 'Succ',
    //             //     lat: data.latitude,
    //             //     lng: data.longitude
    //             // });
    //             res.json(rows);
    //         } else {
    //             res.json({ "greska": null });
    //         }

    //     })
    //     .catch(e => console.log(e))
    //     .finally(console.log("Upit je obraden"));

});

app.get('/profile', requiresAuth(), function (req, res) {
    res.send(JSON.stringify(req.oidc.user));
});

let port = process.env.PORT;
if (port == null || port == "") {
    port = 3000;
    https.createServer(options, app).listen(port, () => {
        console.log(`Slusanje na https://127.0.0.1:${port}/`);
    });
} else {
    app.listen(port, () => {
        console.log(`Slusanje na Heroku na portu:${port}/`);
    });
}


async function execute(data) {
    // const client = new Client({
    //     user: process.env.USER,
    //     password: process.env.PASSWORD,
    //     host: process.env.HOST,
    //     port: 5432,
    //     database: process.env.DATABASE
    // });
    const client = new Client({
        connectionString: process.env.DATABASE_URL,
        ssl: {
            rejectUnauthorized: false
        },
        user: process.env.USER,
        password: process.env.PASSWORD,
        host: process.env.HOST,
        port: 5432,
        database: process.env.DATABASE
    });
    try {
        await client.connect();
        console.log("Connected successfully");
        await client.query("CREATE TABLE IF NOT EXISTS public.logs(id integer serial,username character varying(100),email character varying(100),logintime character varying(100) unique,latitude real,longitude real)")
        // await client.query("insert into logs (username, email, logintime, latitude, longitude) values ($1, $2, $3, $4, $5)", [data.ime, data.email, data.time, data.latitude, data.longitude]);
        const results = await client.query("select * from logs order by logs.id desc limit 5");
        console.table(results.rows);
        return results.rows;
    } catch (err) {
        console.log(`Dogodila se greska ${err}`)
        return 0;
    } finally {
        await client.end();
        console.log("Konekcija se zatvara");
    }


}